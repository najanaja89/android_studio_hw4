package kz.astana.materialapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DrawerLayout dl = findViewById(R.id.drawerLayout);
        NavigationView nv = findViewById(R.id.navigation);
        FrameLayout frameLayout = findViewById(R.id.container);

        LinearLayout ll = findViewById(R.id.llmenu);
        RelativeLayout rl =  findViewById(R.id.rlMenu);
        ConstraintLayout cl = findViewById(R.id.clMenu);

        TextView tv = new TextView(MainActivity.this);
        //tv.setTextSize(30f);
        //frameLayout.addView(ll);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dl.openDrawer(nv);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.hide();
            }
        });

        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.first) {
                    frameLayout.addView(ll);
                } else if (item.getItemId() == R.id.second) {
                    frameLayout.addView(rl);
                } else if (item.getItemId() == R.id.third) {
                    frameLayout.addView(cl);
                    fab.show();
                }
                dl.closeDrawers();
                return true;
            }
        });
    }
}